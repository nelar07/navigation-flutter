import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';



class Two extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TwoState();
  }
}

class _TwoState extends State<Two> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Página Two"),
          centerTitle: true,
        ),
        body: Container(
            padding: EdgeInsets.only(left: 16, top: 25, right: 16),
            child: ListView(children: [
              
              Column(children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      
                  ),
                  child: Text(
                    'ir a Login',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  
                  onPressed: () async {
                    Navigator.pushNamed(context, '/login');
                  },
                ),
                SizedBox(height: 15),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      
                  ),
                  child: Text(
                    'Ir a Página Four',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    Navigator.pushNamed(context, '/four');
                  },
                ),
              ],),
              
            ])),
        bottomNavigationBar: Tabs());
  }
}
