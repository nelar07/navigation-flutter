import 'package:flutter/material.dart';
import 'package:app_service/widgets/one.dart';
import 'package:app_service/widgets/two.dart';
import 'package:app_service/widgets/three.dart';
import 'package:app_service/widgets/four.dart';
import 'package:app_service/widgets/login.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Navigation',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/login': (context)=>Login(),
        '/one': (context)=>One(),
        '/two': (context)=>Two(),
        '/three': (context)=>Three(),
        '/four': (context)=>Four(),
       
      },
      initialRoute: '/login',
    );
  }
}
