import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';



class Four extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FourState();
  }
}

class _FourState extends State<Four> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Página Four"),
          centerTitle: true,
        ),
        body: Container(),
        bottomNavigationBar: Tabs());
  }
}



