
import 'package:flutter/material.dart';


class Tabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TabsState();
  }
}

class _TabsState extends State<Tabs> {
  int _currentIndex = 0;
  
  _TabsState();
  @override
  Widget build(BuildContext context){

    return BottomNavigationBar(
          
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('One'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Two')
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Three'),
            ),
          ],
        );
  }

  void onTabTapped(int index) {
    if(index == 0){
      Navigator.pushNamed(context, '/one');
    } else if(index == 1){
      Navigator.pushNamed(context, '/two');
    } else if(index == 2){
      Navigator.pushNamed(context, '/three');
    }
    setState(() {
      _currentIndex = index;
    });
  }

}

